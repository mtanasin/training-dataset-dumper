#######################################################################
# Here we construct a retagging sequence that takes a few variables
# Such as :
#   the input track, jet collections
#   list of tracking systematics to run over
#   option to merge LRT and standard tracks
# This allows the users to perform retagging with more flexibilities
######################################################################

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from BTagTrainingPreprocessing.trackUtil import LRTMerger, applyTrackSys

from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg
from BTagging.BTagTrackAugmenterAlgConfig import BTagTrackAugmenterAlgCfg
from BTagging.BTagRun3Config import RetagRenameInputContainerCfg
from BTagging.BTagRun3Config import BTagAlgsCfg

def retagging(cfgFlags, merge, input_track = "InDetTrackParticles", input_jet = "AntiKt4EMPFlowJets", sys_list = []):

    acc = ComponentAccumulator()

    pvCol = 'PrimaryVertices'
    jetCol = input_jet
    muon_collection = 'Muons'
    track_collection = input_track

    TrainingMap={
        'AntiKt4EMPFlow': [
           'BTagging/201903/rnnip/antikt4empflow/network.json',
           'BTagging/201903/dl1r/antikt4empflow/network.json',
           'BTagging/20210729/dipsLoose/antikt4empflow/network.json', #new r22 trainings
           'BTagging/20210729/dips/antikt4empflow/network.json',
           'BTagging/20210824r22/dl1dLoose/antikt4empflow/network.json', #recommended tagger which is DL1dLoose20210824r22 named DL1dv00 in EDM
           'BTagging/20210824r22/dl1d/antikt4empflow/network.json',
           'BTagging/20210824r22/dl1r/antikt4empflow/network.json',
        ],
    }

    if sys_list:
        acc.merge(applyTrackSys(sys_list, input_track, input_jet))
        track_collection = "InDetTrackParticles_Sys"

    # The decoration on tracks needs to happen before the track merging step
    # Decorate the standard tracks first

    acc.merge(BTagTrackAugmenterAlgCfg(cfgFlags,  TrackCollection = track_collection, PrimaryVertexCollectionName = pvCol))

    if merge:
        # Decorate the LRT tracks and merge the LRT and standard tracks into a single view collection
        acc.merge(BTagTrackAugmenterAlgCfg(cfgFlags,  TrackCollection = "InDetLargeD0TrackParticles", PrimaryVertexCollectionName = pvCol))
        acc.merge(LRTMerger())
        track_collection = "InDetWithLRTTrackParticles"

    jetCol_no_Jets = jetCol.replace('Jets','')

    acc.merge(RetagRenameInputContainerCfg('_retag',jetCol_no_Jets))
    acc.merge(JetTagCalibCfg(cfgFlags))

    acc.merge(BTagAlgsCfg(
        cfgFlags,
        JetCollection=jetCol_no_Jets,
        nnList=TrainingMap[jetCol_no_Jets],
        trackCollection=track_collection,
        muons=muon_collection,
    ))

    return acc
