#include "HitWriter.hh"
#include "HitDecorator.hh"
#include "HitWriterConfig.hh"
#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

#include "xAODTracking/Vertex.h"
#include "xAODJet/Jet.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"

/////////////////////////////////////////////
// Internal classes
/////////////////////////////////////////////

typedef std::function<float(const HitOutputs&)> FloatFiller;
class HitConsumers: public H5Utils::Consumers<const HitOutputs&> {};
class HitOutputWriter: public H5Utils::Writer<1,const HitOutputs&>
{
public:
  HitOutputWriter(H5::Group& file,
                  const std::string& name,
                  const HitConsumers& cons,
                  size_t size):
    H5Utils::Writer<1,const HitOutputs&>(file, name, cons, {{size}}) {}
};


///////////////////////////////////
// Class definition starts here
///////////////////////////////////
HitWriter::HitWriter(
  H5::Group& output_file,
  const HitWriterConfig& config):
  m_hdf5_hit_writer(nullptr),
  m_dR_to_jet(config.dR_to_jet),
  m_save_endcap_hits(config.save_endcap_hits),
  m_save_only_clean_hits(config.save_only_clean_hits),
  m_bec(Acc<int>("bec"))
{

  HitConsumers fillers;
  add_hit_variables(fillers);

  // build the output dataset
  if (config.name.size() == 0) {
    throw std::logic_error("hit output name not specified");
  }
  if (config.output_size == 0) {
    throw std::logic_error("can't make an output writer with no hits");
  }
  
  m_hdf5_hit_writer.reset(
    new HitOutputWriter(
      output_file, config.name, fillers, config.output_size));
}

HitWriter::~HitWriter() {
  if (m_hdf5_hit_writer) m_hdf5_hit_writer->flush();
}

HitWriter::HitWriter(HitWriter&&) = default;

void HitWriter::write(const std::vector<const xAOD::TrackMeasurementValidation*>& hits,
                      const xAOD::Jet& jet,
		      const xAOD::Vertex& vx) {
  if (m_hdf5_hit_writer) {
    std::vector<HitOutputs> hit_outputs;
    for (const auto hit: hits) {
      // Calculate dR of hit to jet
      TVector3 hitPos(hit->globalX() - vx.x(), hit->globalY() - vx.y(), hit->globalZ() - vx.z());
      bool withinDR = (jet.p4().Vect().DeltaR(hitPos) < m_dR_to_jet);
      
      // Check if the hit should be saved (e.g. EC vs barrel)
      bool isBarrel = (m_bec(*hit) == 0);
      bool saveHit = isBarrel || (!isBarrel && m_save_endcap_hits);
            
      // Only save hits that we want to save
      if (withinDR && saveHit) {
        hit_outputs.push_back(HitOutputs{hit, &jet, &vx});
      }
    }

    m_hdf5_hit_writer->fill(hit_outputs);
  }
}

void HitWriter::write_dummy() {
  if (m_hdf5_hit_writer) {
    std::vector<HitOutputs> hit_outputs;
    m_hdf5_hit_writer->fill(hit_outputs);
  }
}

void HitWriter::add_hit_variables(HitConsumers& vars) {
  
  // local and global hit positions
  vars.add("x_local", [](const HitOutputs& h){ return h.hit->globalX() - h.pv->x(); }, NAN);
  vars.add("y_local", [](const HitOutputs& h){ return h.hit->globalY() - h.pv->y(); }, NAN);
  vars.add("z_local", [](const HitOutputs& h){ return h.hit->globalZ() - h.pv->z(); }, NAN);
  vars.add("x_global", [](const HitOutputs& h){ return h.hit->globalX(); }, NAN);
  vars.add("y_global", [](const HitOutputs& h){ return h.hit->globalY(); }, NAN);
  vars.add("z_global", [](const HitOutputs& h){ return h.hit->globalZ(); }, NAN);
  
  // Hit layer (integer 0-4) and isBarrel (bec=0: barrel, bec=+-2: endcap)
  vars.add("layer", [acc=Acc<int>("layer")](const HitOutputs& h){ return acc(*h.hit); }, -1);
  vars.add("isBarrel", [acc=Acc<int>("bec")](const HitOutputs& h){ return (acc(*h.hit) == 0); }, false);
  
  // Information about hit quality
  vars.add("isFake", [acc=Acc<char>("isFake")](const HitOutputs& h){ return acc(*h.hit); }, -1);
  vars.add("broken", [acc=Acc<char>("broken")](const HitOutputs& h){ return acc(*h.hit); }, -1);
  vars.add("DCSState", [acc=Acc<int>("DCSState")](const HitOutputs& h){ return acc(*h.hit); }, -1);
  vars.add("hasBSError", [acc=Acc<int>("hasBSError")](const HitOutputs& h){ return acc(*h.hit); }, -1);
          
  // Information about split hits
  vars.add("isSplit", [acc=Acc<int>("isSplit")](const HitOutputs& h) { return acc(*h.hit); }, -1);  
  vars.add("splitProbability1", [acc=Acc<float>("splitProbability1")](const HitOutputs& h){ return acc(*h.hit); }, NAN);
  vars.add("splitProbability2", [acc=Acc<float>("splitProbability2")](const HitOutputs& h){ return acc(*h.hit); }, NAN);

}
