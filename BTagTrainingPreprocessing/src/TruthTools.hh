#ifndef TRUTH_TOOLS_HH
#define TRUTH_TOOLS_HH

#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"


namespace truth {
  class TruthRecordError: public std::runtime_error
  {
    using std::runtime_error::runtime_error;
  };
  
  bool isFromWZ(const xAOD::TruthParticle& truth_particle);
  
  bool isOverlapingLepton(
    const xAOD::Jet& jet,
    const std::vector<const xAOD::TruthParticle*>& truth_particles, float dR
  );
  
  bool passed_truth_jet_matching(
    const xAOD::Jet& jet,
    const xAOD::JetContainer& truthjets);
  
  std::vector<const xAOD::TruthParticle*> getLeptonsFromWZ(
    const std::vector<const xAOD::TruthParticle*>&
  );

  const xAOD::TruthParticle* getParentHadron(const xAOD::TruthParticle* truth_particle);
  bool isWeaklyDecayingHadron(const xAOD::TruthParticle& truth_particle, int flavour);
  bool isHadron(const xAOD::TruthParticle& truth_particle, int flavour);
  bool isFinalStateChargedLepton(const xAOD::TruthParticle& truth_particle);

  std::vector<int> getDRSortedIndices(
    std::vector<const xAOD::TruthParticle*> ConeAssocHad, 
    const xAOD::Jet &jet
  );

  void getAllChildren(
    const xAOD::TruthParticle* particle, 
    std::vector<const xAOD::TruthParticle*> &truthFromPart
  );

}

#endif
