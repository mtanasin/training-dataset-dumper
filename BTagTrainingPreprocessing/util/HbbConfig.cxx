#include "HbbConfig.hh"
#include "ConfigFileTools.hh"

#define BOOST_BIND_GLOBAL_PLACEHOLDERS // ignore deprecated ptree issues
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <filesystem>

HbbConfig get_hbb_config(const std::string& config_file_name) {
  namespace cft = ConfigFileTools;
  namespace fs = std::filesystem;
  using boost::property_tree::ptree;
  HbbConfig config;

  fs::path cfg_path(config_file_name);
  if (!fs::exists(cfg_path)) {
    throw std::runtime_error(cfg_path.string() + " doesn't exist");
  }
  std::ifstream cfg_stream(cfg_path.string());
  auto nlocfg = nlohmann::ordered_json::parse(cfg_stream);
  cft::combine_files(nlocfg, cfg_path.parent_path());

  // convert back to ptree for now, but new code should use nlocfg if
  // possible!
  auto pt = cft::from_nlohmann(nlocfg);

  for (auto& subnode: pt.get_child("subjets")) {
    auto& subjet = subnode.second;
    SubjetConfig cfg;
    cfg.input_name = subjet.get<std::string>("input_name");
    cfg.output_name = subjet.get<std::string>("output_name");
    cfg.get_subjets_from_parent = cft::boolinate(
      subjet,"get_subjets_from_parent");
    cfg.n_subjets_to_save = subjet.get<size_t>("n_subjets_to_save");
    cfg.n_tracks = subjet.get<size_t>("n_tracks");
    cfg.min_jet_pt = subjet.get<double>("min_jet_pt");
    cfg.num_const = subjet.get<size_t>("num_const");
    // read in the b-tagging variables
    cfg.variables = cft::get_variable_list(subjet.get_child("variables"));
    cfg.track_variables = cft::get_track_variables(
      pt.get_child("track_variables"));

    // add DL2 configs
    for (const auto& nn_cfg: nlocfg.at("dl2_configs")) {
      cfg.dl2_configs.push_back(cft::get_dl2_config(nn_cfg));
    }

    cfg.skip_if_missing_tracks = cft::boolinate_default(
      subjet, "skip_if_missing_tracks");

    config.subjet_configs.push_back(cfg);
  }
  config.jet_collection = pt.get<std::string>("jet_collection");
  config.jet_calib_file = pt.get<std::string>("jet_calib_file");
  config.cal_seq = pt.get<std::string>("cal_seq");
  config.cal_area = pt.get<std::string>("cal_area");
  config.n_const = pt.get<size_t>("n_const");
  config.top_tag_config = pt.get<std::string>("top_tag_config");
  for (const auto& cfg: pt.get_child("hbb_tag_config")) {
    config.hbb_tag_config.push_back(cfg.second.get_value<std::string>());
  }

  config.truth_wz_electron_container = pt.get<std::string>(
    "truth_wz_electron_container");

  config.variables = cft::get_variable_list(pt.get_child("variables"));

  return config;
}
