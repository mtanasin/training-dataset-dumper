#
# Basic setup for dumping Tagging information
#

# Set the minimum required CMake version:
cmake_minimum_required( VERSION 3.14 FATAL_ERROR )

project( Dumper VERSION 1.0 LANGUAGES CXX C )

# figure out what release we're in
if (NOT "$ENV{AnalysisBase_DIR}" STREQUAL "")
  set(ATLAS_PROJECT AnalysisBase)
  find_package( ${ATLAS_PROJECT} 22.2 REQUIRED )
elseif (NOT "$ENV{Athena_DIR}" STREQUAL "")
  set(ATLAS_PROJECT Athena)
  find_package( ${ATLAS_PROJECT} 23.0 REQUIRED )
elseif (NOT "$ENV{AthAnalysis_DIR}" STREQUAL "")
  set(ATLAS_PROJECT AthAnalysis )
  find_package( ${ATLAS_PROJECT} 22.2 REQUIRED )
endif()
if (NOT DEFINED ATLAS_PROJECT)
  message(FATAL_ERROR "Can't figure out release")
endif()

# Set up CTest:
atlas_ctest_setup()

# # Set up a work directory project:
atlas_project( Dumper 1.0 USE ${ATLAS_PROJECT} ${${ATLAS_PROJECT}_VERSION} )

# Set up the runtime environment setup script(s):
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )

# Set up CPack:
atlas_cpack_setup()
